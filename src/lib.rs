use std::fs;
use std::error::Error;
use std::io::BufReader;
use std::path::Path;

use oggvorbismeta::VorbisComments;

/// Enum to allow either oggvorbismeta::CommentHeader or audiotags::traits::AudioTag
pub enum AudioFile {
    /// Specifically for ogg format because audiotags does not support ogg
    Ogg(oggvorbismeta::CommentHeader),
    /// All other formats (mp3, m4a, and flac)
    Other(Box<dyn audiotags::traits::AudioTag>),
}

impl AudioFile {
    /// Instantiate a new AudioFile when given a path, using the file's extension to decide if it's
    /// Self::Ogg or Self::Other
    pub fn from(path: &Path) -> Result<Self, Box<dyn Error>> {
        if path.extension().ok_or("No extension")? == "ogg" {
            Ok(Self::Ogg(oggvorbismeta::read_comment_header(BufReader::new(fs::File::open(path)?))))
        } else {
            Ok(Self::Other(audiotags::Tag::default().read_from_path(path)?))
        }
    }

    /// Get a tag from the audio file using a string. Ogg gets the tags using the string as well,
    /// while other formats can only get artist, album, tracknumber, title, and date tags.
    pub fn get<T: ToString>(&self, tag: T) -> Result<String, Box<dyn Error>> {
        let tag = tag.to_string();
        match self {
            Self::Ogg(ch) => Ok(ch.get_tag_single(&tag).ok_or(format!("No {} tag", tag))?),
            Self::Other(at) => {
                match tag.to_lowercase().as_str() {
                    "artist" => Ok(at.artists().ok_or("No artist tag")?.get(0).unwrap().to_string()),
                    "album" => Ok(at.album_title().ok_or("No album title tag")?.to_string()),
                    "tracknumber" => Ok(at.track_number().ok_or("No track number tag")?.to_string()),
                    "title" => Ok(at.title().ok_or("No artist tag")?.to_string()),
                    "date" => Ok(at.year().ok_or("No artist tag")?.to_string()),
                    _ => None.ok_or(format!("No {} tag", tag))?,
                }
            },
        }
    }
}
